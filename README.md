# angular-app-pipeline

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.
It contains a demonstration to build a fully featured GitLab pipeline.

Here is a brief description of the pipeline jobs.

## install_dependencies
Dedicated job to install node dependencies and compile Angular depencencies for Ivy. The resulting `node_modules` is cached for the next jobs and pipelines. As the cache key is one `yarn.lock` file, the job only runs when this file is modified.

All other jobs have either no cache or the pull cache policy.
This ensure the others jobs won't modify and push the cache.

## test_app
It includes first the linting step and the test step.
This job also produces tests and coverage reports.

It is volontary runned in parallel with `build_app` job because they can an their execution time is similar.

Linting step runs before tests but inside the same job.
There are multiple reasons to not parallelise with tests:
* it would use an additional runner for only a ~8 sec task
* the additionnal job would also load docker executor and cache (~25 sec)
* the pipeline will fail only after all other parrallel jobs are over 

Yet, this solution have drawbacks:
* `test_app` job is ~8 sec longer
* when the job fail it can be due to either the lint or test

Like others jobs, it uses an image from the project docker registry. The job uses a custom node image to avoid the ~30 sec overhead when installing Chrome.

## build_app
A this step, the app is build and provided through the job artifact.

It's one of the slower jobs but it must be runned during all pipelines. While it can't be cached like `insall_dependencies`, it should be one of the latest to run.

## publish_container
In this app scenario, there are two enviroments for deployment.
`dev` doesn't exist but it is implemented for demonstration.
`prod` do exists and leverage GitLab pages.

There is no need to build a docker image for GitLab pages as it asks for the app artifact. This job purpose is to demo how to use project docker registry.

A docker image containing the build is produced for `develop` and `master` branches as well as release tags. Only release images are tagged as latest because this is the default when pulling from the registry without specifying a tag.

Note the Dockerfile is copied during `build_app` job in app artifact. The context to build the image is keeped as light as possible.

## pages (aka deploy_prod)
Merging master don't always means deployment in production. The pipeline only activates this job when a new tag is pushed.

This job is manual, meaning the user have to run it through the UI. This is because the version is also deployed on dev environment first. It gives to the person in charge of the deployment a chance for a last check.

GitLab environments are used, so it's possible to have an overview of previous deployments.

## update_ci_images
In this scenario, this isn't a big optimisation but it saves job time for sure. All docker images used by jobs with docker executor are hosted on the project docker registry.

It's represents a very little improvment for Gitlab runner which takes ~10 sec more to pull images from docker.io registry.

For the `test_app` job, there is no docker image with chromium pre-installed. Using a node:alpine image and install it during `before_script` would slow the job by ~30 sec at each run. This is more efficient to build a complete image before-hand.

The images used by the pipeline are defined in `.ci/Dockerfile` which is a multi-stage Dockerfile for convenience. Whenever this file is changed, the pipeline adds this first job to build the images again and push them to the project docker registry. Then, the following jobs in the pipeline can use the updated images. 
